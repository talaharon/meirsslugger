export default function slugger(str){
    return str.split(' ').join('-');
}